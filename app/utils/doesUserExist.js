const { api } = require('@rocket.chat/sdk')

const doesUserExist = async username => {
    let success = false;
    let result = await api.get('users.info', { username })

    if (result != undefined) {
        success = result.success;
    }

    return success;
}

module.exports = doesUserExist