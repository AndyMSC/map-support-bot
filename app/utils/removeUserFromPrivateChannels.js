const { api } = require('@rocket.chat/sdk')

const removeUserFromPrivateChannel = require('./removeUserFromPrivateChannel')

const removeUserFromPrivateChannels = async (user, channels) => {
  let kicks = []
  for (let channel of channels) {
    kicks.push(await removeUserFromPrivateChannel(user, channel))
  }
  return kicks
}

module.exports = removeUserFromPrivateChannels