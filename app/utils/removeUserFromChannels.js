const { api } = require('@rocket.chat/sdk')

const removeUserFromChannel = require('./removeUserFromChannel')

const removeUserFromChannels = async (user, channels) => {
  let kicks = []
  for (let channel of channels) {
    kicks.push(await removeUserFromChannel(user, channel))
  }
  return kicks
}

module.exports = removeUserFromChannels
