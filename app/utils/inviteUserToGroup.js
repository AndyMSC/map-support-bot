const { api } = require('@rocket.chat/sdk')

const inviteUserToGroup = async (user, group) => {
  try {
    let result = await api.post('groups.invite', {
      roomId: group._id,
      userId: user._id,
    })
    if (result.success) {
      return result.group
    } else {
      console.error(result)
    }
  } catch (error) {
    console.error(result)
    return error
  }
}

module.exports = inviteUserToGroup