const { api } = require('@rocket.chat/sdk')

const reactToMessage = async (emoji, message, shouldReact = true) => {
  let result = await api.post('chat.react', {
    emoji,
    messageId: message._id,
    shouldReact,
  })
  if (result.success) {
    return result.role
  } else {
    console.error(result)
  }
}

module.exports = reactToMessage
