const { api } = require('@rocket.chat/sdk')

const getGroupByGroupName = async (groupName) => {
    let result = await api.get('groups.info', { roomName: groupName })
    if (result.success) {
      return result.group
    } else {
      console.error(result)
    }
  }

  module.exports = getGroupByGroupName