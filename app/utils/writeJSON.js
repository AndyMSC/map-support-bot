const fs = require('fs')

const writeJSON = (file, data) => {
  fs.writeFile(file, JSON.stringify(data), err => {
    if (err) throw err
  })
}

module.exports = writeJSON
