const { api } = require('@rocket.chat/sdk')

const getUserByUsername = require('./getUserByUsername')

const updateUserName = async (currentUserName, newUserName) => {
    const user = await getUserByUsername(currentUserName);

    let result = await api.post('users.update', {
        userId: user._id,
        data: {
          username: newUserName,
        },
      })
      
      if (result.success) {
        return result.user
      } else {
        console.error(result)
      }
}

module.exports = updateUserName