const { api } = require('@rocket.chat/sdk')

const getChannelByChannelID = async (ID) => {
  let result = await api.get('channels.info', { roomId: ID })
  if (result.success) {
    return result.channel
  } else {
    console.error(result)
  }
}

module.exports = getChannelByChannelID
