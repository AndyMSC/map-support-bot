const { api } = require('@rocket.chat/sdk')

const getUsersList = async (object) => {
  let result = await api.get('users.list', object)
  if (result && result.success) {
    return result
  } else {
    console.error(result)
  }
}

module.exports = getUsersList
