const { api } = require('@rocket.chat/sdk')

const deleteMessage = async (roomId, msgId, asUser = false) => {
  let result = await api.post('chat.delete', { roomId, msgId, asUser })
  if (result.success) {
    return result.channel
  } else {
    console.error(result)
  }
}

module.exports = deleteMessage
