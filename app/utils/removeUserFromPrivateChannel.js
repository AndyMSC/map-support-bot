const { api } = require('@rocket.chat/sdk')

const removeUserFromPrivateChannel = async (user, channel) => {
  try {
    let result = await api.post('groups.kick', {
      roomName: channel.name,
      userId: user._id,
    })
    if (result.success) {
      return result.channel
    } else {
      console.error(result)
    }
  } catch (error) {
    console.error(result)
    return error
  }
}

module.exports = removeUserFromPrivateChannel
