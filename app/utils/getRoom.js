const { api } = require('@rocket.chat/sdk')

const getRoom = async (room) => {
  let result = await api.get('rooms.info', { ...room })
  if (result && result.success) {
    return result.room
  } else {
    console.error(result)
  }
}

module.exports = getRoom
