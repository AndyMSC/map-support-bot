const { api } = require('@rocket.chat/sdk')

const getRoom = require('./getRoom')

const getRooms = async (rooms) => {
  let _rooms = []
  for (let room of rooms) {
    _rooms.push(await getRoom(room))
  }
  return _rooms
}

module.exports = getRooms
