const { api } = require('@rocket.chat/sdk')

const inviteUserToGroup = require('./inviteUserToGroup')

const inviteUserToGroups = async (user, groups) => {
  let invites = []
  for (let group of groups) {
    invites.push(await inviteUserToGroup(user, group))
  }
  return invites
}

module.exports = inviteUserToGroups
