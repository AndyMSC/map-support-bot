const addmap = require('./addMAP')
const onboard = require('./onboard')
const userinfo = require('./userInfo')
const changeusername = require('./changeUserName')

module.exports = {
  commands: { 'Guide Commands': { addmap, onboard, userinfo, changeusername } },
}
