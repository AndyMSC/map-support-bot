const dayjs = require('dayjs')

const getUserByUsername = require('../../utils/getUserByUsername')
const getUserInfo = require('../../utils/getUserInfo')
const isMinor = require('../../utils/isMinor')

const emailsToString = (emails) => {
  let result = ''
  emails.forEach((email) => {
    result = `${result} ${email.address}`
    if (email.verified) {
      result = `${result} ✔️`
    } else {
      result = `${result} ❌`
    }
  })

  return result
}

async function userInfo({ bot, message, context }) {
  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)
  // Get the targetUser object
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Don't show email until we have a flag for it
  // Emails: ${emailsToString(targetUser.emails)}

  let userInfo = await getUserInfo(targetUser)

  let response = `
${'```'}
      Type:             ${userInfo.type}
      Name:             ${targetUser.name}
      Username:         ${targetUser.username}
      Roles:            ${targetUser.roles}

      Status:           ${userInfo.status}
      statusConnection: ${userInfo.statusConnection}
      
      Number of Rooms:  ${userInfo.rooms.length}
      utcOffset:        ${userInfo.utcOffset}
      Active:           ${userInfo.active}
      User ID:          ${targetUser._id}

      Status Text:      ${userInfo.statusText}

      Last Login:       ${dayjs(targetUser.lastLogin).toString()}
      Created At:       ${dayjs(userInfo.createdAt).toString()}
${'```'}
      `
  // Minor?:           ${isMinor(targetUser) ? '✔️' : '❌'}

  const sentMsg = await bot.sendToRoom(response, roomID)
}

module.exports = {
  description: 'Get information about a specific member.',
  help: `${process.env.ROCKETCHAT_PREFIX} userInfo <username>`,
  requireOneOfRoles: ['admin', 'Global Moderator', 'Guide'],
  call: userInfo,
}
