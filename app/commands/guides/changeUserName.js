const updateUserName = require('../../utils/updateUserName')
const doesUserExist = require('../../utils/doesUserExist')

async function changeusername({ bot, message, context }) {
    const roomID = message.rid
    let originalUserName = context.argumentList[0];
    let newUserName = context.argumentList[1];

    let userExist = await doesUserExist(originalUserName);

    if (userExist == false) {
      originalUserName = "changename-" + originalUserName;
      userExist = await doesUserExist(originalUserName);
    }

    if (userExist)
    {
      await updateUserName(originalUserName, newUserName);

      await bot.sendToRoom(
          'Updated ' + originalUserName + ' to ' + newUserName,
          roomID,
        )
    } else {
      await bot.sendToRoom(
        'User ' + context.argumentList[0] + ' does not exist!',
        roomID,
      );
    }
}

module.exports = {
    description:
      'Change a user\'s username to another ',
    help: `${process.env.ROCKETCHAT_PREFIX} changeusername <current username> <new username>`,
    requireOneOfRoles: ['admin', 'Global Moderator', 'Guide'],
    call: changeusername,
  }