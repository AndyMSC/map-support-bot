const roominfo = require('./roomInfo')
const testcommand = require('./testCommand')
const testcooldown = require('./testCooldown')

module.exports = {
  commands: { 'Developer Commands': { roominfo, testcommand, testcooldown } },
}
