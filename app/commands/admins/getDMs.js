const dayjs = require('dayjs')

const getUserByUsername = require('../../utils/getUserByUsername')
const getDirectMessages = require('../../utils/getDirectMessages')
const upload = require('../../utils/upload')

async function getDMs({ bot, message, context }) {
  const roomID = message.rid
  let targetUsers = [...context.argumentList]

  const result = await getDirectMessages(
    {
      query: { usernames: { $all: targetUsers } },
    },
    {
      count: 0,
      sort: { _updatedAt: 1 },
    },
  )

  // Send the message

  let message_sent = await bot.sendMessage({
    rid: roomID,
    attachments: [
      {
        title: `DMs between: ${targetUsers}`,
        collapsed: true,
        fields: result.messages.map((message) => ({
          short: false,
          title: `${message.u.username} - ${dayjs(
            message._updatedAt,
          ).toString()}`,
          value: `${message.msg}`,
        })),
      },
    ],
  })
}

module.exports = {
  description: 'get Direct Messages',
  help: `${process.env.ROCKETCHAT_PREFIX} getDMs <username>`,
  requireOneOfRoles: ['admin'],
  call: getDMs,
}
