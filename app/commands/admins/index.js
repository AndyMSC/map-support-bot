const addawa = require('./addAWA')
const addminor = require('./addMinor')
const deleteminor = require('./deleteMinor')
const getdms = require('./getDMs')
const addrole = require('./addRole')

module.exports = {
  commands: { 'Administrator Commands': { addawa, addrole } },
}
