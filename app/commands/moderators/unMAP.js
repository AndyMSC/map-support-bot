const getUserByUsername = require('../../utils/getUserByUsername')

const removeUserFromRoles = require('../../utils/removeUserFromRoles')
const userHasRole = require('../../utils/userHasRole')

const removeUserFromChannels = require('../../utils/removeUserFromChannels')
const inviteUserToChannel = require('../../utils/inviteUserToChannel')
const getUserInfo = require('../../utils/getUserInfo')
const getRooms = require('../../utils/getRooms')

const prescreeningID = 'yCJaDGjDmrc3RXuaE'

async function unMAP({ bot, message, context }) {
  const roomID = message.rid
  const user = await getUserByUsername(message.u.username)

  // Get the targetUser object
  let targetUser = context.argumentList[0]
  targetUser = await getUserByUsername(targetUser)

  // Check if the targetUser does already have the MAP role
  if (userHasRole(targetUser, 'MAP')) {
    // Remove the roles from targetUser
    await bot.sendToRoom(
      'Removing roles from ' + targetUser.name + ' ...',
      roomID,
    )
    const rolesToRemove = targetUser.roles.filter((role) => role !== 'user')
    await removeUserFromRoles(targetUser, rolesToRemove)

    const userInfo = await getUserInfo(targetUser)

    // Don't kick the user from direct messages
    let userRoomsToKickFrom = userInfo.rooms.filter((room) => room.t !== 'd' && room.t !== 'p')

    // Format the list to work with the getRooms function
    userRoomsToKickFrom = await getRooms(
      userRoomsToKickFrom.map((room) => ({ roomId: room.rid })),
    )

    // Filter out undefined
    userRoomsToKickFrom = userRoomsToKickFrom.filter(
      (room) => room !== undefined,
    )

    // Don't kick the user from default channels
    userRoomsToKickFrom = userRoomsToKickFrom.filter((room) => !room.default)

    await bot.sendToRoom(
      'Removing ' +
        targetUser.name +
        ` from all rooms except direct messages and default channels (Total: ${userRoomsToKickFrom.length})...`,
      roomID,
    )

    let kicks = await removeUserFromChannels(targetUser, userRoomsToKickFrom)
    kicks.forEach((kick) => {
      console.log('THIS IS A KICK OR AN ERROR FOR DEBUGGING:', kick)
    })

    // Add the user to pre-screening
    let invite = await inviteUserToChannel(targetUser, { _id: prescreeningID })

    await bot.sendToRoom(
      'Finished removing ' + targetUser.name + ' from all the rooms! 🔨',
      roomID,
    )
  } else {
    // User does not have the MAP role, no need to do anything else
    const response = targetUser.name + " doesn't have the MAP role"
    const sentMsg = await bot.sendToRoom(response, roomID)
    return
  }
}

module.exports = {
  description:
    'Remove the member from the map role and kick them from the appropriate channels.',
  help: `${process.env.ROCKETCHAT_PREFIX} unMAP <username>`,
  requireOneOfRoles: ['admin', 'Global Moderator'],
  call: unMAP,
}
