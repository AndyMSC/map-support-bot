const boop = require('./boop')
const define = require('./define')
const feedback = require('./feedback')
const hug = require('./hug')
const poll = require('./poll')
const trivia = require('./trivia')
const headpat = require('./headpat')
const advice = require('./advice')
const icanhazdad = require('./icanhazdad')
const dog = require('./dog')
const cat = require('./cat')
const mention = require('./mention')
const staff = require('./staff')
const eightball = require('./eightball')

module.exports = {
  commands: {
    'Member Commands': {
      boop,
      define,
      feedback,
      hug,
      poll,
      trivia,
      headpat,
      advice,
      icanhazdad,
      dog,
      cat,
      mention,
      staff,
      eightball,
    },
  },
}
