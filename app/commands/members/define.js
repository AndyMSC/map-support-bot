const levenshtein = require('js-levenshtein')

const definitions = {
  AAM: 'Adult-Attracted Minor, someone who finds adults attractive.',
  ABDL:
    'Adult Baby/Diaper Lover. An AB is a type of age-player who desires to be treated like a baby, including sometimes dressing up in diapers and other clothes. A DL is someone who likes wearing and/or using diapers, but not necessarily with an age-play element.',
  Anti: 'Someone opposed to pedophiles/MAPs.',
  'Anti-contact':
    'An ideology among some minor-attracted persons (MAPs) that sexual activity between minors and adults would still carry a fundamental risk for harm that outweighs potential benefits even if it were to be legalized and accepted in society. MAP Support Club was founded on this ideology.',
  AoA:
    'Age of Attraction, the ages a person finds sexually or romantically attractive, usually given as a range.',
Aromantic: 
     'Someone who doesn\'t experience romantic attraction. They may experience sexual attraction or may not. Some aromantics choose to be in romantic relationships, while some choose not to.',
Asexual: 
     'Someone who is asexual lacks sexual attraction. This is separate from action. Some asexual people are sex-repulsed, some are sex-neutral, and some have and enjoy sex for various reasons. Like many things, asexuality is a spectrum, which includes:\nDemisexual: \'People who only experience sexual attraction after making a strong emotional connection with a specific person.\'\nGreysexual: \'People experience limited sexual attraction.\'\nAutochorissexual: \'People who get aroused by sexual content but not actually want to engage in any sexual activities. They masturbate, but are neutral or repulsed by the idea of having sex with another person\'\nAutopedophilia: \'Erotic pleasure from imagining oneself to be a child.\'', 
Demisexual: 'People who only experience sexual attraction after making a strong emotional connection with a specific person.',
Greysexual: 'People experience limited sexual attraction.',
Autochorissexual: 'People who get aroused by sexual content but not actually want to engage in any sexual activities. They masturbate, but are neutral or repulsed by the idea of having sex with another person.\'',
Autopedophilia: 'Erotic pleasure from imagining oneself to be a child.',
Bisexual: 
     'Attraction to two or more genders. This includes trans and nonbinary people and is not limited to two genders, although bi does technically mean two.',  
BA: 'Boy attracted, a boy-oriented MAP.',
BL: 'Boy lover, a boy-oriented MAP.',
  CBT:
    'Cognitive Behavioral Therapy. A popular "talk" therapy based on identifying the thoughts behind unwanted feelings and changing them.',
  'Cheese Pizza': 'Euphemism for child pornography. (C)heese (P)izza / (C)hild (P)ornography',
  CA: 'Child attracted, a MAP who is attracted to children of more than one gender.',
  CL: 'Child lover, a MAP who is attracted to children of more than one gender.',
  COCSA: 'Child on child sexual abuse',
  CP: 'Child pornography.',
  CSA: 'Child sexual abuse.',
  CSAM:
    'Child sexual abuse material. A term for child pornography that emphasizes the abusive nature of real photos/videos of children in sexual situations.',
  CSEM:
    'Child sex exploitation material. A term for child pornography that emphasizes disapproval.',
  Ephebophilia:
    'Sexual attraction to late-pubescent or post-pubescent minors, an attraction within the MAP umbrella.',
  'Exclusive (pedophile)':
    'Attracted only to children and not at all to adults.',
  GA: 'Girl attracted, a girl-oriented MAP.',
  GL: 'Girl lover, a girl-oriented MAP.',
  Ghosting: 'Abandoning someone with zero further contact.',
  Hebephilia:
    'Sexual attraction to early-pubescent children, an attraction within the MAP umbrella.',
  IIOC:
    'Illegal Images of Children, a term that is used primarily in the UK to refer to child pornography.',
LBL:
    'Little-boy lover, a MAP who is predominantly attracted to boys under the age of 4-8.',
  LGF:
    'Little-girl friend. Used similarly to YF (young friend) but more commonly used among GLs.',
  LGL:
    'Little-girl lover, a MAP who is predominantly attracted to girls under the age of 4-8.',
  MAP:
    'Minor-Attracted Person. An umbrella term to \'pedophile\' that some prefer because of less associated stigma. "Minor" is a legal term so it includes older children, encompassing hebephilia and ephebophilia as well as pedophilia and nepiophilia.',
  MIP:
    'Minor-identifying person. Collective term for autopedophiles (or autohebes, autonepio etc), ageplayers, adult babies, regressors, those with age dysphoria, trans-age folks and those who in some other way identify with or as children/minors.',
  Muggle:
    'In the Harry Potter series of books, muggles are those that aren\'t capable of doing magic. That term has been adapted by many communities to refer to those that do not belong to that community. In our case, a muggle is a non-pedophile. Some also use the word "normies", but muggle is cooler and you should use that instead.',
  Nepiophilia: 'Sexual attraction to infants and toddlers.',
  NOMAP: 'Non-Offending Minor-Attracted Person.',
  Non:
    'A non-pedophile, usually applied to those who are sympathetic to us. A muggle ally.',
Nonbinary:   'Someone whose gender is something other than male or female. Nonbinary people may or may not identify as transgender.',  
  'Non-Exclusive (pedophile)': 'Attracted to children but also to adults.',
Pansexual:   'Attraction to all genders, or regardless of gender.',  
Pedophilia:
    'Sexual attraction to pre-pubescent children. Often also used to refer to sexual attraction to minors in general. Since the beginning of puberty varies from child to child, no fixed age can be given, but under 11 years old is a rough approximation. The term is also incorrectly used by the public and press to mean illegal sexual activity with children and underage teens.',
  'Pedophilic Disorder':
    'Pedophilic Disorder (PD). A psychiatric illness diagnosed by a medical professional when 1) a pedophile experiences recurrent, clinically-significant distress as a direct result of their sexual orientation OR 2) a pedophile engages in illegal sexual activity with a child.',
  POCD:
    'Pedophilic Obsessive-Compulsive Disorder (OCD). An obsessive worry that a person might be a pedophile when they do not actually have a strong sexual attraction to children. A type of OCD.',
  PPD:
    'Prevention Project Dunkelfeld, the German therapy project for non-offending pedophiles. See https://dont-offend.org for more information.',
  PPG:
    "Penile plethysmograph. A device that measures how much a man's penis grows when exposed to various stimuli. Used to diagnose pedophilia (and homosexuality).",
  'Pro-contact':
    'An ideology among some minor-attracted persons (MAPs) that consensual sexual activity between minors and adults would be fundamentally okay if there was no risk of harm from societal stigma or from legal ramifications.',
  SYF: 'Special young friend.',
  TBL: 'Teen boy lover, hebephile attracted to teenage boys.',
  Teleiophilia: 'Sexual attraction to adults. Describes most ordinary people.',
  TGL: 'Teen girl lover, hebephile attracted to teenage girls.',
Transgender: 'Someone who does not identify with the sex they were born with for their gender identity. Gender identities can include a wide array beyond man/woman.',
Transsexual: 'This refers to someone who is transgender and has physically transitioned to the gender they identify as. Some view this term as outdated and stigmatizing, especially towards those who are not able to transition.',
Transvestite: 'This term used to mean cross-dresser, though is now viewed by some in the trans community as a stigmatizing term.',
  'Westermarck Effect':
    'Describes how siblings that grow up together from infancy do not generally develop sexual attraction to each other. While not originally applied to pedophilia, most pedophiles feel no sexual attraction towards their own children, as long as they have raised the children from early infancy. Sometimes the term is used to cover this case too.',
  YF:
    'Young Friend, a child a pedophile knows personally and is fond of, but may or may not be sexually attracted to.',
}

async function define({ bot, message, context }) {
  const roomID = message.rid
  let requestedDefinition = context.argumentList[0]
  // Get all the keys into a list
  definition_terms = Object.keys(definitions)

  console.log('\n\n\n\n', requestedDefinition)

  if (requestedDefinition === undefined || requestedDefinition === '') {
    let message = '#### The MAP Dictionary \n```xml\n'
    definition_terms.forEach((term) => {
      message = message + `|   ${term}\n`
    })
    message = message + '```'
    const sentMsg = await bot.sendToRoom(message, roomID)
  } else {
    // Calculate how similar the requested definition is to each definition key,
    // Using the Levenshtein algorithm.
    definition_keys_objects = definition_terms.map((term) => {
      return {
        term,
        distance: levenshtein(
          requestedDefinition.toLowerCase(),
          term.toLowerCase(),
        ),
      }
    })

    // Sort according to the distance
    definition_keys_objects = definition_keys_objects.sort(
      (a, b) => a.distance - b.distance,
    )

    for (let i = 0; i < 3; i++) {
      let term_object = definition_keys_objects[i]

      // Get the actual definition
      let definition = definitions[term_object.term]

      // Send the response
      const sentMsg = await bot.sendToRoom(
        `**${term_object.term}:**\n${definition}`,
        roomID,
      )

      // Break the loop if the term is very similar to the requested term
      if (term_object.distance < 5) {
        break
      }
    }
  }
}

module.exports = {
  description: "MSC's dictionary of MAP relevant terms.",
  help: `${process.env.ROCKETCHAT_PREFIX} define <term (optional)>`,
  call: define,
}
