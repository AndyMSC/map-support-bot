const fetch = require('isomorphic-fetch')

async function icanhazdad({ bot, message, context }) {
  const roomID = message.rid

  fetch('https://icanhazdadjoke.com/', {
    headers: { Accept: 'application/json' },
  })
    .then(async (response) => {
      console.log(response)
      if (response.status == 200) {
        return response.json()
      }
    })
    .then(async (joke) => await bot.sendToRoom(joke.joke, roomID))

  // Send the message
  //message =
}

module.exports = {
  description: 'Get a dad joke!',
  help: `${process.env.ROCKETCHAT_PREFIX} icanhazdad`,
  cooldown: 30,
  call: icanhazdad,
}
