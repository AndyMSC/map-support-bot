const fetch = require('isomorphic-fetch')

async function dog({ bot, message, context }) {
  const roomID = message.rid
  const roomName = message.roomName

  // Require command to be called in #spam or #comfy-cafe
  if (!['spam','comfy-cafe'].includes(roomName)) {
    await bot.sendToRoom('Command needs to be called in #spam or #comfy-cafe', roomID)
    return
  }

  fetch('https://dog.ceo/api/breeds/image/random', {
    headers: { Accept: 'application/json' },
  })
    .then(async (response) => {
      if (response.status == 200) {
        return response.json()
      }
    })
    .then(async (dog) => {
      let message = await bot.prepareMessage(
        {
          attachments: [
            {
              image_url: dog.message,
            },
          ],
        },
        roomID,
      )

      await bot.sendMessage(message)
    })
}

module.exports = {
  description:
    'Posts a random Dog 🐶 image! Needs to be called in #spam or #comfy-cafe. Images provided by: dog.ceo/dog-api/',
  help: `${process.env.ROCKETCHAT_PREFIX} dog`,
  cooldown: 30,
  call: dog,
}
