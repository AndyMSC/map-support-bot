const responses = [
    //affirmative answers
    "It is certain.",
    "It is decidedly so.",
    "Without a doubt.",
    "Yes definitely.",
    "You may rely on it.",
    "As I see it, yes.",
    "Most likely.",
    "Outlook good.",
    "Yes.",
    "Signs point to yes.",

    //non-committal answers
    "Reply hazy, try again.",
    "Ask again later.",
    "Better not tell you now.",
    "Cannot predict now.",
    "Concentrate and ask again.",

    //negative answers
    "Don't count on it.",
    "My reply is no.",
    "My sources say no.",
    "Outlook not so good.",
    "Very doubtful."
]

async function eightball({ bot, message, context }) {
  const roomID = message.rid
  rand = Math.floor(Math.random() * responses.length)
  let message = responses[rand]
  // Send the message
  message = await bot.sendToRoom(message, roomID)
}

module.exports = {
  description: 'Run this command with a question and await your fortune! 🎱',
  help: `${process.env.ROCKETCHAT_PREFIX} eightball`,
  call: eightball,
}
