const fetch = require('isomorphic-fetch')

async function cat({ bot, message, context }) {
  const roomID = message.rid
  const roomName = message.roomName

  // Require command to be called in #spam or #comfy-cafe
  if (!['spam','comfy-cafe'].includes(roomName)) {
    await bot.sendToRoom('Command needs to be called in #spam or #comfy-cafe', roomID)
    return
  }

  fetch('https://api.thecatapi.com/v1/images/search', {
    headers: { Accept: 'application/json' },
  })
    .then(async (response) => {
      if (response.status == 200) {
        return response.json()
      }
    })
    .then(async (cats) => {
      console.log(cats)
      let message = await bot.prepareMessage(
        {
          attachments: [
            {
              image_url: cats[0].url,
            },
          ],
        },
        roomID,
      )

      await bot.sendMessage(message)
    })
}

module.exports = {
  description:
    'Posts a random cat 😺 image! Needs to be called in #spam or #comfy-cafe. Images provided by: thecatapi.com/',
  help: `${process.env.ROCKETCHAT_PREFIX} cat`,
  cooldown: 30,
  call: cat,
}
